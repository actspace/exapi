

var co = require('co');
var server = require('./lib/SERV');
var db = require('./lib/DB');

var appName = process.argv[2];
var portNumber = process.argv[3];


// 必須要有埠號指定,測試用不要80 port ...
if (!portNumber) {
  console.log('沒有必要執行參數:埠號');
  process.exit(0);
}

if (portNumber === 80) {
  console.log('埠號不能為80');
  process.exit(0);
}

//啟動偵聽port號
server.start(parseInt(portNumber, 10), function () {
  console.log(appName + ' start at ' + portNumber);
});

//http get
server.get('/data', function (req, res) {
  var fn = co.wrap(function* (rtnObj) {
    //回傳的物件
    rtnObj = {
      passObj:{},
      outObj:{
        result: 'fail',
        res: '未知錯誤',
        data: null,
        totalCount: 0
      }
    };
    
    //要執行的步驟順序
    var proc = [
      step1,  //必要參數檢查
      step2,  //跳頁參數正確性檢查
      step3   //取資料
    ];
    
    //逐項執行
    for (var key in proc) {
      var func = proc[key];
      rtnObj = yield func(req, rtnObj);
      if (rtnObj.outObj.result != 'ok') return rtnObj;
    }
    
    return rtnObj;
  });
  
  
  //結果輸出
  fn(true).then(function (rtnObj) {
    res.send(rtnObj.outObj);
  });

});


function step1 (req, rtnObj) {
  return function (callback) {
    var currentPage = req.query.currentpage;
    var pageSize = req.query.pagesize;
    
    if (!currentPage || !pageSize) {
      rtnObj.outObj.result = 'fail';
      rtnObj.outObj.res = '缺少參數(currentpage/pagesize)';
    } else {
      rtnObj.outObj.result = 'ok';
    }
    
    callback(null, rtnObj);
  };
}


function step2 (req, rtnObj) {
  return function (callback) {
    
    var currentPage = parseInt(req.query.currentpage, 10);
    var pageSize = parseInt(req.query.pagesize, 10);
    
    rtnObj.outObj.result = 'ok';
    
    if (isNaN(currentPage) || isNaN(pageSize)) {
      rtnObj.outObj.result = 'fail';
      rtnObj.outObj.res = '參數必須為數字';
    }
    
    if (currentPage < 1 || pageSize < 1) {
      rtnObj.outObj.result = 'fail';
      rtnObj.outObj.res = '參數不能少於1';
    }    
    
    callback(null, rtnObj);
  };
}

function step3 (req, rtnObj) {
  return function (callback) {
    
    var currentPage = parseInt(req.query.currentpage, 10);
    var pageSize = parseInt(req.query.pagesize, 10);
    
    var sql = 'SELECT f.id, f.first_name, f.last_name, f.email, f.gender FROM friends as f LIMIT ?, ?;';

    db.query(sql, [((currentPage - 1) * pageSize), pageSize], function (err, rows) {
      if (err) {
        rtnObj.outObj.result = 'fail';
        rtnObj.outObj.res = '資料庫錯誤,請聯絡管理員';
      } else {
        rtnObj.outObj.result = 'ok';
        rtnObj.outObj.res = '請求完成';
        rtnObj.outObj.data = rows;
      }

      //取總筆數
      if (rtnObj.outObj.result == 'ok') {
        db.query('SELECT COUNT(1) as totalCount FROM friends as f;', [], function (err, rows) {
          if (rows.length == 0 || err) {
            rtnObj.outObj.result = 'fail';
            rtnObj.outObj.res = '資料庫錯誤,請聯絡管理員';
          } else {
            rtnObj.outObj.totalCount = rows[0].totalCount;
          }
          callback(null, rtnObj);
        });
      }
    });
  };
}
