//***************************************
// 資料庫存取相關
// 建檔人員: Andersen
// 建檔日期: 2016/01/30
// 功能說明: 
// 關聯程式: 
//***************************************
'use strict';

var mysql = require('mysql');

// 建構
function DbConstructor(){
	this.conn = {
		host: '172.10.0.1',
		user: 'root',
		password: '2369oiceuthn',
		port: 3306,
		database: 'testdb',
		multipleStatements: true
	};
	this.pool = mysql.createPool(this.conn);
}

/**
 * 取連線字串
 *
 * @returns {object} connection object
 */
DbConstructor.prototype.getConn = function() {
	return this.conn;
};


/**
 * 設定連線字串
 *
 * @param {object} connection object
 *
 * @returns {boolean} true=new setting
 */
DbConstructor.prototype.setConn = function(newConn) {
	var result = false;
	if (newConn) {
		this.conn = newConn;
		result = true;
	}
	return result;
};


DbConstructor.prototype.query = function() {
	var args = arguments;
	this.pool.getConnection(function(err, connection) {
		if (err) {
			console.log(err);
			if (connection) {
				connection.release();
			}
		}
		else {
			connection.query.apply(connection, args);
			connection.release();
		}
	});
};

module.exports = exports = new DbConstructor();

// 使用範例
// var sql = "SELECT 1 as ok " +
//           "FROM api_partner_info i " +
//           "JOIN api_game_mapping g ON i.partnerCode = g.partnerCode AND i.gameId = g.gameId " +
//           "WHERE i.deleted = 0 AND g.active = 1 AND i.partnerCode = ? AND i.gameId = ? AND g.intoServer = ? AND g.createDate <= UNIX_TIMESTAMP()";
// db.query(sql, [partner, gameid, serverid], function (err, rows) {
//   if (rows.length == 0 || err) {
//     rtnObj.outObj.result = 'error';
//     rtnObj.outObj.errCode = 'E01';   //服務未開通
//   }
//   callback(null, rtnObj);
// });