var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var fs = require('fs');
var path = require('path');
// var security = require('./SECURITY');
// var tools = require('./TOOLS');

var app = express();

app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

  if (req.method == 'OPTIONS') {
    res.send(200); // 让options请求快速返回
  }
  else {
    next();
  }
});

app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

exports.get = function () {
  var args = arguments;
  app.get.apply(app, args);
};
exports.post = function () {
  var args = arguments;
  app.post.apply(app, args);
};
exports.put = function () {
  var args = arguments;
  app.put.apply(app, args);
};
exports.del = function () {
  var args = arguments;
  app.delete.apply(app, args);
};

exports.cls = function(){
  process.exit(0);
};

exports.start = function (portNumber, callback) {
  app.listen(portNumber, '0.0.0.0', callback);
};